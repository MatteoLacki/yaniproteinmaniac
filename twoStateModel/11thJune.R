library(dplyr)
library(seqinr)
library(tidyr)
library(ggplot2)
# library(broom)
# library(MASS)

setwd('~/Dropbox/Science/Yani/proteinUnfolding/data/11thJune')
source('~/Dropbox/Science/Yani/proteinUnfolding/twoStateModel/findMeT.R')

dataTypes <- c('chap','bulk')

Results <- lapply(
	dataTypes,
	function(dataType)
		lapply(
			list.files(dataType),
			function(dN){
				path <- paste0(dataType,'/',dN)
				times<- sapply(
					paste0(path,'/',list.files(path)),
					function(x) read.table(x)$V1 %>% findMeT,
					USE.NAMES = FALSE
				)

				data.frame(
					times 	= times,
					temp 	= as.integer(c2s(s2c(dN)[1:3])),
					type 	= dataType
				)
			}
		) %>% unnest
) %>% unnest

Results 	<- Results %>% split( interaction(Results$type, Results$temp) )

# normTimes 	<- Results[[1]] %>% select( times ) %>% unlist(use.names=FALSE)
# maxTime 	<- max(normTimes)
# normTimes 	<- normTimes/maxTime
# averageNormTime <- mean(normTimes)

# x <- c(5,1)
# x <- c(720,5)

likelihood <- function(x){
	mu 		<- x[1]		
	lambda 	<- x[2]

	blibli 	<- (mu - lambda) * normTimes
	blah 	<- exp(blibli) - 1
	
	res 	<- (log(lambda)+log(mu)-log(mu-lambda)) - mu * averageNormTime 
	
	infiniteValues 	<- is.infinite(blah)
	if(any(infiniteValues)){
		
		return(
			res +
			( 
				sum(
					log(blah[!infiniteValues])
				) + 
				sum(
					blibli[infiniteValues]
				)
			) / length(normTimes) 
		)	
	} else 
	return(
		res + mean(log(blah))
	)


}
antiLikelihood <- function(x) -likelihood(x)
# antiLikelihood(x)

gradient 	<- function(x){
	mu 		<- x[1]		
	lambda 	<- x[2]

	averageExp <- mean( ( 1 - exp( (lambda - mu) * normTimes ) )^{-1}* normTimes )
	
	c(
		1/mu 	- 1/(mu-lambda) + averageExp,
		1/lambda+ 1/(mu-lambda) - averageExp
	)
}
antiGradient <- function(x) -gradient(x)
# antiGradient(x)

antiLikelihood(c(1000,5))
antiGradient(c(1000,5))

NMopt <- optim(
	par = c(50,2),
	fn 	= antiLikelihood,
	gr 	= antiGradient,
	method = "Nelder-Mead"
)
likelihood( NMopt$par )
# gradient( NMopt$par )
# likelihood(c(714.7,5))


# NMopt <- optim(
# 	par = c(5000,50),
# 	fn 	= antiLikelihood,
# 	gr 	= antiGradient,
# 	method = "Nelder-Mead"
# )


(NMopt$par/maxTime)^{-1}
# Tlist <- list(a = 1,b ='adfasfag', c= function(x) x^2 ) 
# Tlist$a <- 2
# Tlist[[3]]
# Tlist$c(Tlist$a)

NMopt$par

BFGSopt <- optim(
	par = c(50,2),
	fn 	= antiLikelihood,
	gr 	= antiGradient,
	method = "BFGS"
)
likelihood(BFGSopt$par)


CGopt <- optim(
	par = c(1000,5),
	fn 	= antiLikelihood,
	gr 	= antiGradient,
	method = "CG"
)
likelihood(CGopt$par)


L_BFGS_Bopt <- optim(
	par = c(500,1),
	fn 	= antiLikelihood,
	gr 	= antiGradient,	
	method = "L-BFGS-B"
)
likelihood(L_BFGS_Bopt$par)
(L_BFGS_Bopt$par/maxTime)^{-1}



lapply(
	c( "Nelder-Mead", "BFGS", "CG", "L-BFGS-B"),
	function( method )
		try(
			optim(
				par 	= c(500,1),
				fn 		= antiLikelihood,
				gr 		= antiGradient,	
				method 	= method 
			)
		)
)

# SANNopt <- optim(
# 	par = c(500,1),
# 	fn 	= antiLikelihood,
# 	gr 	= antiGradient,	
# 	method = "SANN"
# )
# likelihood(SANNopt$par)
# likelihood(x)


survival <- function(grid, x)
{
	mu 		<- x[1]		
	lambda 	<- x[2]

	(mu*exp(-lambda*grid) - lambda * exp( - mu * grid))/(mu-lambda)
}

makePlot <- function(MaxGrid)
	plot(
		0:MaxGrid,
		survival(0:MaxGrid, NMopt$par/maxTime  ),
		ylim = c(0,1),	
		type = 'l'
	)
makePlot(maxTime)
