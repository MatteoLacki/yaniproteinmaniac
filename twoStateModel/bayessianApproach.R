 ggplot(faithful, aes(x=waiting, y=..density..)) +
        geom_histogram(fill="cornsilk", colour="grey60", size=.2) +
        geom_density() +
        xlim(35, 105)

        ?geom_density