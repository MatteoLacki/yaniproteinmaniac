#include <boost/math/special_functions/binomial.hpp>
#include <Rcpp.h>
#define   LOGBINOM(n, k)  log(boost::math::binomial_coefficient<double>(n, k))
using namespace Rcpp;

// [[Rcpp::export]]
double money( unsigned n, unsigned k )
{
	return LOGBINOM(n, k);
}
