#include <Rcpp.h>
#include <boost/math/special_functions/binomial.hpp>
#define   LOGBINOM(n, k)  log(boost::math::binomial_coefficient<double>(n, k))

using namespace Rcpp;

// [[Rcpp::export]]
NumericVector   findBetterT( 
  NumericVector CumSumX, 
  NumericVector revCumSumX, 
  double p 
){
  using   namespace std;
  int     n = CumSumX.length(); 
  double  logp = log(p), log1mp = log(1-p); 

  NumericVector likelihood(n);

  for( int i = 1; i < n; ++i )
  {
      // No of zeros before the breakage point.
    int zerosBeforeT= i-1 - CumSumX[i-1];
      // No of ones after the breakage point.
    int onesAfterT  = revCumSumX[i];

    likelihood[i-1] = 
      ;

      logNewton( i-1, zerosBeforeT ) + 
      zerosBeforeT * logp + 
      (i-1-zerosBeforeT) * log1mp +  
      logNewton( n - i + 1, onesAfterT ) +  
      onesAfterT * logp +
      ( n - i + 1 - onesAfterT ) * log1mp;

      cout << i << endl;
  }

  return likelihood;
} 