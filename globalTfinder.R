library(Rcpp)
sourceCpp('~/Dropbox/Science/proteinUnfolding/maxLikelihoodEstimate.cpp')
x <- dataSets[[1]][[13]]

findBetterT(
	csx <- cumsum(x),
	rev(csx),
	.001		
)

library(Rcpp)
sourceCpp('~/Dropbox/Science/proteinUnfolding/boostTest.cpp')

library(microbenchmark)
microbenchmark(
	money(900,30),
	choose(900,30)
)

